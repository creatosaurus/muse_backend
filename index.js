const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const logger = require('./logger');
const app = express();

// connect to the data base
const connectToDataBase = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/muse',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        poolSize: 10, // set the size of the connection pool
        keepAlive: true, // enable TCP keepalive
        keepAliveInitialDelay: 300000 // wait 5 minutes before sending the first keepalive message
      }
    );
    logger.info('muse database connection success');
  } catch (error) {
    logger.error(error)
  }
};

connectToDataBase();
//middle wares
app.use(cors());
app.options('*', cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: '100mb' }));

app.use('/muse/images', require('./Routes/Images'));
app.use('/muse/elements', require('./Routes/Elements'));
app.use('/muse/templates', require('./Routes/Templates'));
app.use('/muse/uploads', require('./Routes/UserUploads'));
app.use('/muse/admintemplates', require('./Routes/AdminTemplates'));
app.use('/muse/group', require('./Routes/Group'));
app.use("/muse/stability", require('./Routes/StabilityAi'))
app.use("/muse/flux", require('./Routes/AiFlux'))
app.use("/muse/image/edit", require('./Routes/ModelsLabImageEditing'))
app.use("/muse/user", require('./Routes/UserDelete'))

module.exports = app;
