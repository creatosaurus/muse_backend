const S3 = require('aws-sdk/clients/s3');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');
const { google } = require('googleapis');
const templateImages = require('../Schema/TemplateImages');
const bucketName = 'creatosaurus-muse';
const region = 'ap-south-1';
const accessKeyId = 'AKIAQ5RO3HORQNA4HO5I';
const secretAccessKey = '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';

const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey,
});

//upload a file to s3
const generateuploadURLToS3 = async () => {
  try {
    const imageName = uuidv4();
    const expireSecondsOfURL = 60 * 30;

    const params = {
      Bucket: bucketName,
      Key: imageName,
      Expires: expireSecondsOfURL,
    };

    const uploadURL = await s3.getSignedUrlPromise('putObject', params);
    return { uploadURL, imageName };
  } catch (error) {
    console.log(error);
  }
};

const downloadTheImageFromUrlAndStoreToS3 = async (url, userId) => {
  try {
    const image = await axios.get(url, { responseType: 'stream' });
    const { data } = image;

    const params = {
      Bucket: bucketName,
      Key: uuidv4(),
      Body: data,
      ContentType: image.headers['content-type'],
    };

    const { Location, key } = await s3.upload(params).promise();

    // create the template object
    const object = {
      userId: userId,
      originalURL: url,
      s3URL: Location,
      fileName: key,
    };

    await templateImages.create(object);
    return {
      oldURL: url,
      newURL: Location,
    };
  } catch (error) {
    return {
      oldURL: url,
      newURL: url,
    };
  }
};

const addBase64DataToS3Bucket = async (base64) => {
  try {
    const base64Data = new Buffer.from(
      base64.replace(/^data:image\/\w+;base64,/, ''),
      'base64'
    );
    const type = base64.split(';')[0].split('/')[1];

    const params = {
      Bucket: bucketName,
      Key: uuidv4(),
      Body: base64Data,
      ContentEncoding: 'base64',
      ContentType: `image/${type}`,
    };

    const { Location } = await s3.upload(params).promise();
    return Location;
  } catch (error) {
    return base64;
  }
};


const addAiImageToBucket = async (buffer, uuid, contentType) => {
  try {
    const params = {
      Bucket: bucketName,
      Key: uuid,
      Body: buffer,
      ContentType: contentType,
    };

    const { Location } = await s3.upload(params).promise();
    return Location;
  } catch (error) {
    console.error("Error uploading to S3:", error);
    return null;
  }
};

// delete icon
const deleteOldFile = async (key) => {
  try {
    const params = { Bucket: bucketName, Key: key };
    await s3.deleteObject(params).promise();
    return "deleted";
  } catch (error) {
    return "error";
  }
};

// download and store drive image to s3
const oauth2Client = new google.auth.OAuth2(
  "148537948078-v9ofeuph3fe7gq8t9o9obi8d5odg080c.apps.googleusercontent.com",
  "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",
  "https://api.cache.creatosaurus.io/cache/google/drive/auth/callback"
);

const downloadDriveImagesAndStoreToS3 = async (fileId, access_token, contentType, contentLength) => {
  try {
    oauth2Client.credentials = {
      access_token: access_token,
    };
    const service = google.drive({ version: 'v3', auth: oauth2Client });
    const file = await service.files.get({
      fileId: fileId,
      alt: 'media',
    }, { responseType: 'stream' });

    const params = {
      Bucket: bucketName,
      Key: uuidv4(),
      Body: file.data,
      ContentType: contentType,
    };

    const { Location, Key } = await s3.upload(params).promise();

    return {
      id: Key,
      URL: Location,
      contentType: contentType,
      contentLength: contentLength
    };

  } catch (error) {
    console.log(error);
    return "invalid url";
  }
};

module.exports = {
  generateuploadURLToS3,
  downloadTheImageFromUrlAndStoreToS3,
  addBase64DataToS3Bucket,
  addAiImageToBucket,
  deleteOldFile,
  downloadDriveImagesAndStoreToS3,
};
