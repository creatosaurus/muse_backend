const axios = require("axios")

const checkAiImageCreditsAreThere = async (req) => {
    try {
        const token = req.headers.authorization.split(" ")[1]
        const config = { headers: { "Authorization": `Bearer ${token}` } }

        const res = await axios.post("https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/ai-image/check/credit", {
            workspaceOwnerUserId: req.body.organizationUserId
        }, config)

        const { message } = res.data
        if (parseInt(message) > 0) return true
        return false
    } catch (error) {
        return false
    }
}

const removeImageAiCredits = async (req, model) => {
    try {
        const token = req.headers.authorization.split(" ")[1]
        const config = { headers: { "Authorization": `Bearer ${token}` } }

        const oneCreditModels = ["core", "remove-bg", "upscale"]

        const removeCredit = oneCreditModels.includes(model) ? 1 : 2

        await axios.post("https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/ai-image/remove/credit", {
            workspaceOwnerUserId: req.body.organizationUserId,
            removeCredit
        }, config)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    checkAiImageCreditsAreThere,
    removeImageAiCredits
}