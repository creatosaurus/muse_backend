const express = require('express')
const path = require('path')
const userUploads = require('../Schema/UserUploadsSchema')
const router = express.Router()
const { generateuploadURLToS3, deleteOldFile } = require('../Functions/S3')
const logger = require('../logger');
const userStorage = require('../Schema/UserStorage')
const aiImages = require('../Schema/AiImages')
const checkAuth = require('../Middleware/CheckAuth')

router.post('/', async (req, res) => {
    try {
        let savedImageObject
        const userStorageData = await userStorage.findOne({ userId: req.body.userId })
        if (userStorageData === null) {
            let data = {
                userId: req.body.userId,
                storageSize: req.body.fileSize
            }
            await userStorage.create(data)
            savedImageObject = await userUploads.create(req.body)
        } else {
            // check storage is full or not for user
            let storageLimit
            let storageSize
            if (req.body.storage === undefined || req.body.storage === null) {
                storageLimit = 1000000000
                storageSize = 1
            } else {
                storageLimit = 1000000000 * parseInt(req.body.storage)
                storageSize = parseInt(req.body.storage)
            }

            if (parseInt(userStorageData.storageSize) + req.body.fileSize > storageLimit) {
                return res.status(507).json({ error: `You have reached your ${storageSize} gb storage limit` })
            }

            userStorageData.storageSize = parseInt(userStorageData.storageSize) + req.body.fileSize
            await userStorageData.save()
            savedImageObject = await userUploads.create(req.body)
        }
        res.send(savedImageObject)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get('/generate/url', async (req, res) => {
    try {
        const data = await generateuploadURLToS3()
        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get('/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const searchQuery = req.query.query || '';
        let userUploadedImages = []

        if (searchQuery) {
            userUploadedImages = await userUploads.find({
                organizationId: req.params.organizationId,
                isDeleted: { $ne: true },
                userFileName: { $regex: new RegExp(searchQuery, 'i') }
            }).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else {
            userUploadedImages = await userUploads.find({
                organizationId: req.params.organizationId,
                isDeleted: { $ne: true }
            }).sort({ $natural: -1 }).skip(skip).limit(limit)
        }

        res.send(userUploadedImages)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

// get trash images
router.get('/image/trash/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const userUploadsImages = await userUploads.find({ organizationId: req.params.organizationId, isDeleted: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(userUploadsImages)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})


// user uploads add to trash
router.delete('/image/trash/:id', checkAuth, async (req, res) => {
    try {
        const imageData = await userUploads.findById(req.params.id);

        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        // Mark the image as deleted (soft delete)
        imageData.isDeleted = true;
        await imageData.save();

        // Return a 200 OK response confirming the deletion
        return res.status(200).json({ message: "Image successfully deleted" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
})

// user uploads delete permanantely
router.delete("/image/:id/delete-permanently", checkAuth, async (req, res) => {
    try {
        // Find the AI-generated image in the database using the provided ID
        const imageData = await userUploads.findById(req.params.id);

        // If the image is not found, return a 404 Not Found response
        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        await deleteOldFile(imageData.fileName);
        await userUploads.deleteOne({ _id: imageData._id });
        return res.status(200).json({ message: "Image permanently deleted." });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
});

// user uploads restore image
router.post("/image/:id/restore", checkAuth, async (req, res) => {
    try {
        // Find the AI-generated image in the database using the provided ID
        const imageData = await userUploads.findById(req.params.id);

        // If the image is not found, return a 404 Not Found response
        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        imageData.isDeleted = false;
        await imageData.save();

        return res.status(200).json({ message: "Image successfully deleted" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
});

router.get('/ai-images/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const searchQuery = req.query.query || '';
        let userGeneratedAiImages = []

        if (searchQuery) {
            userGeneratedAiImages = await aiImages.find({
                organizationId: req.params.organizationId,
                isDeleted: { $ne: true },
                prompt: { $regex: new RegExp(searchQuery, 'i') }
            }).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else {
            userGeneratedAiImages = await aiImages.find({
                organizationId: req.params.organizationId,
                isDeleted: { $ne: true }
            }).sort({ $natural: -1 }).skip(skip).limit(limit)
        }

        res.send(userGeneratedAiImages)
    } catch (error) {
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get('/trash/ai-images/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const userUploadsImages = await aiImages.find({ organizationId: req.params.organizationId, isDeleted: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(userUploadsImages)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get("/search/:organizationId", async (req, res) => {
    try {
        const searchRegex = new RegExp(req.query.query, 'i');
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        const data = await userUploads.find({
            organizationId: req.params.organizationId,
            delete: false,
            $or: [
                { fileName: { $regex: searchRegex } },
                { userFileName: { $regex: searchRegex } }
            ]
        }).sort({ $natural: -1 }).skip(skip).limit(limit)

        res.send(data)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get("/storage/limit/:userId", async (req, res) => {
    try {
        const userStorageData = await userStorage.findOne({ userId: req.params.userId })
        res.send(userStorageData)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.get('/media/:id', async (req, res) => {
    try {
        const media = await userUploads.findById(req.params.id)
        const filePath = path.join(__dirname, `/../Uploads/${media.fileName}`)
        res.sendFile(filePath)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

router.delete('/media/:id', async (req, res) => {
    try {
        const media = await userUploads.findById(req.params.id)
        media.delete = true
        await media.save()

        const userData = await userStorage.findOne({ userId: media.userId })
        if (!userData) return res.send("deleted")

        userData.storageSize = parseInt(userData.storageSize) - media.fileSize
        await userData.save()

        res.send("deleted")
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
})


router.post("/favorites/toggle/:id", checkAuth, async (req, res) => {
    try {
        const isDataThere = await userUploads.findById(req.params.id)
        if (!isDataThere) return res.status(404).json({ message: "Image data not found" });

        if (isDataThere.isFavorite) {
            isDataThere.isFavorite = false
        } else {
            isDataThere.isFavorite = true
        }

        const data = await isDataThere.save()
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error })
    }
});


// get favorite ai images
router.get('/favorites/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 25
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const userUploadsImages = await aiImages.find({ organizationId: req.params.organizationId, isFavorite: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(userUploadsImages)
    } catch (error) {
        res.status(500).json({
            error: "Internal server error"
        })
    }
})

// get favorite uploaded images
router.get('/favorites/media/:organizationId', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 25
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const userUploadsImages = await userUploads.find({ organizationId: req.params.organizationId, isFavorite: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(userUploadsImages)
    } catch (error) {
        res.status(500).json({
            error: "Internal server error"
        })
    }
})


module.exports = router