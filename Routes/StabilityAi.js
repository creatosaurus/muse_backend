const express = require('express');
const axios = require('axios');
const path = require("path")
const FormData = require("form-data")
const sharp = require('sharp')
const fs = require("fs")
const { v4: uuidv4 } = require('uuid');
const checkAuth = require("../Middleware/CheckAuth");
const { addAiImageToBucket, deleteOldFile } = require('../Functions/S3');
const AiImages = require('../Schema/AiImages');
const { checkAiImageCreditsAreThere, removeImageAiCredits } = require('../Functions/AiImageCredits');
const { Readable } = require('stream');

const router = express.Router();

const NEGATIVE_PROMPT = "cartoon, illustration, 3D render, unrealistic, low quality, low resolution, blurred, overexposed, distorted, artificial lighting, plastic texture, oversaturated colors, exaggerated proportions, noise, grain, watermark, text, logo, abstract shapes, unrealistic colors, artificial shadows, deformed hands, extra fingers, extra limbs, fused fingers, missing fingers, deformed legs, distorted feet, unnatural poses, twisted limbs, asymmetrical ears, incorrect ear placement, extra ears, distorted faces, extra arms, elongated neck, warped anatomy, unnatural body proportions, disfigured features";


const pollResult = async (generationID) => {
    try {
        let attempt = 0;
        const maxAttempts = 5;

        while (attempt < maxAttempts) {
            await new Promise(resolve => setTimeout(resolve, 5000)); // Sleep for 5 seconds

            // GET request to check the result status
            const response = await axios.get(`https://api.stability.ai/v2beta/results/${generationID}`, {
                validateStatus: undefined,
                responseType: "arraybuffer",
                headers: {
                    Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                    Accept: "*/*",
                },
            });

            if (response.status === 202) {
                console.log("Generation is still running, try again in 10 seconds.");
            } else if (response.status === 200) {
                return response.data
            } else {
                return null
            }
        }

        return null
    } catch (error) {
        console.log(error)
        return null
    }
};

// Helper function to create a readable stream from a Buffer
function bufferToStream(buffer) {
    const stream = new Readable();
    stream.push(buffer);
    stream.push(null); // Signal end of the stream
    return stream;
}

const compressImage = async (buffer) => {
    try {
        return await sharp(buffer)
            .resize({ width: 1024 }) // Resize to a specific width
            .jpeg({ quality: 80 }) // Adjust quality to reduce size
            .toBuffer();
    } catch (error) {
        console.error("Error compressing image:", error);
        throw error;
    }
};

const compressImageWithoutHeightWidth = async (buffer) => {
    try {
        return await sharp(buffer)
            .jpeg({ quality: 80 }) // Adjust quality to reduce size
            .toBuffer();
    } catch (error) {
        console.error("Error compressing image:", error);
        throw error;
    }
};


router.post("/generate/style", checkAuth, async (req, res) => {
    try {
        const { prompt, model = "core", style = "realistic", organizationUserId, userData, organizationId } = req.body;
        const isCreditsThere = await checkAiImageCreditsAreThere(req)
        if (!isCreditsThere) return res.status(429).json({ message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account." })

        const payload = new FormData();
        payload.append("prompt", prompt);
        payload.append("output_format", "jpeg");
        if (style !== "realistic") {
            payload.append("style_preset", style);
        }

        const response = await axios.post(`https://api.stability.ai/v2beta/stable-image/generate/${model}`, payload, {
            validateStatus: undefined,
            responseType: "arraybuffer",
            headers: {
                Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                Accept: "image/*",
                ...payload.getHeaders(),
            }
        })

        if (response.status !== 200) {
            throw new Error(`${response.status}: ${response.data.toString()}`);
        }

        // Process image buffer and upload concurrently
        const fileName = "ai" + uuidv4();
        let buffer = await compressImage(Buffer.from(response.data));

        const { size, width, height, format } = await sharp(buffer).metadata();
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        // Consolidated image info to store in the database
        const info = {
            organizationUserId: organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            prompt,
            url,
            model: model,
            dimensions: { width, height },
            format,
            fileName,
            fileSize: size,
        };

        const data = await AiImages.create(info);

        const infoData = {
            _id: data._id,
            url: url,
            fileName,
        }

        removeImageAiCredits(req, model)
        res.send([infoData]);
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})

router.post("/generate-image", checkAuth, async (req, res) => {
    try {
        const { prompt, model = "core", organizationUserId, userData, organizationId } = req.body;
        const isCreditsThere = await checkAiImageCreditsAreThere(req)
        if (!isCreditsThere) return res.status(429).json({ message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account." })

        const payload = new FormData();
        payload.append("prompt", prompt);
        payload.append("output_format", "jpeg");
        payload.append("negative_prompt", NEGATIVE_PROMPT);

        const response = await axios.post(`https://api.stability.ai/v2beta/stable-image/generate/${model}`, payload, {
            validateStatus: undefined,
            responseType: "arraybuffer",
            headers: {
                Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                Accept: "image/*",
                ...payload.getHeaders(),
            }
        })

        if (response.status !== 200) {
            throw new Error(`${response.status}: ${response.data.toString()}`);
        }

        // Process image buffer and upload concurrently
        const fileName = "ai" + uuidv4();
        let buffer = await compressImage(Buffer.from(response.data));

        const { size, width, height, format } = await sharp(buffer).metadata();
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        // Consolidated image info to store in the database
        const info = {
            organizationUserId: organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            prompt,
            url,
            model: model,
            dimensions: { width, height },
            format,
            fileName,
            fileSize: size,
        };

        const data = await AiImages.create(info);

        const infoData = {
            _id: data._id,
            url: url,
            fileName,
        }

        removeImageAiCredits(req, model)
        res.send([infoData]);
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})

router.post("/remove-bg", checkAuth, async (req, res) => {
    try {
        const { inputUrl, organizationUserId, userData, organizationId } = req.body;

        // Check credits availability
        if (!(await checkAiImageCreditsAreThere(req))) {
            return res.status(429).json({
                message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account."
            });
        }

        // Fetch the image from inputUrl as a buffer to save memory
        const imageResponse = await axios.get(inputUrl, { responseType: 'arraybuffer' })
        const imageBuffer = Buffer.from(imageResponse.data);

        // Set up form data for API request
        const formData = new FormData();
        formData.append("image", imageBuffer, {
            filename: 'image.jpg',
            contentType: imageResponse.headers['content-type']
        });
        formData.append("output_format", "png");

        // Remove background via API call
        const apiResponse = await axios.post(`https://api.stability.ai/v2beta/stable-image/edit/remove-background`, formData, {
            validateStatus: undefined,
            responseType: "arraybuffer",
            headers: {
                Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                Accept: "image/*",
                ...formData.getHeaders(), // Include appropriate headers for multipart data
            },
        })

        // Validate API response
        if (apiResponse.status !== 200) {
            throw new Error(`${apiResponse.status}: ${Buffer.from(apiResponse.data).toString()}`);
        }

        const buffer = Buffer.from(apiResponse.data);
        const fileName = "ai" + uuidv4();

        // Process image and upload concurrently
        const [metadata, url] = await Promise.all([
            sharp(buffer).metadata(),
            addAiImageToBucket(buffer, fileName, `image/png`)
        ]);

        const model = "remove-bg"

        // Consolidate image info for database
        const data = await AiImages.create({
            organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            inputUrl,
            url,
            model: model,
            dimensions: { width: metadata.width, height: metadata.height },
            format: metadata.format,
            fileName,
            fileSize: metadata.size,
        });

        // Remove credits and respond
        removeImageAiCredits(req, model);
        res.send([{
            _id: data._id,
            url,
            fileName
        }]);
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})


router.post("/upscale", checkAuth, async (req, res) => {
    try {
        const { inputUrl, organizationUserId, userData, organizationId } = req.body;

        // Check credits availability
        if (!(await checkAiImageCreditsAreThere(req))) {
            return res.status(429).json({
                message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account."
            });
        }

        // Fetch the image from inputUrl as a buffer to save memory
        const imageResponse = await axios.get(inputUrl, { responseType: 'arraybuffer' })
        const imageBuffer = Buffer.from(imageResponse.data);

        // Set up form data for API request
        const formData = new FormData();
        formData.append("image", imageBuffer, {
            filename: 'image.jpg',
            contentType: imageResponse.headers['content-type']
        });

        const inputImageFormat = imageResponse.headers['content-type'].split("/")[1]
        formData.append("output_format", inputImageFormat);

        // Remove background via API call
        const apiResponse = await axios.post(`https://api.stability.ai/v2beta/stable-image/upscale/fast`, formData, {
            validateStatus: undefined,
            responseType: "arraybuffer",
            headers: {
                Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                Accept: "image/*",
                ...formData.getHeaders(), // Include appropriate headers for multipart data
            },
        })

        // Validate API response
        if (apiResponse.status !== 200) {
            throw new Error(`${apiResponse.status}: ${Buffer.from(apiResponse.data).toString()}`);
        }

        let buffer = Buffer.from(apiResponse.data);
        const fileName = "ai" + uuidv4();

        // Process image and upload concurrently
        buffer = await compressImageWithoutHeightWidth(buffer)
        const { size, width, height, format } = await sharp(buffer).metadata();
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        const model = "upscale"

        // Consolidate image info for database
        const data = await AiImages.create({
            organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            inputUrl,
            url,
            model: model,
            dimensions: { width: width, height: height },
            format: format,
            fileName,
            fileSize: size,
        });

        // Remove credits and respond
        removeImageAiCredits(req, model);
        res.send([{
            _id: data._id,
            url,
            fileName
        }]);
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})


router.post("/erase", checkAuth, async (req, res) => {
    try {
        const { originalImage, maskImage, organizationUserId, userData, organizationId } = req.body;

        // Check credits availability
        if (!(await checkAiImageCreditsAreThere(req))) {
            return res.status(429).json({
                message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account."
            });
        }

        const base64Data1 = originalImage.split(",")[1];
        const imageBuffer1 = Buffer.from(base64Data1, "base64");
        const imageStream1 = bufferToStream(imageBuffer1);

        const base64Data2 = maskImage.split(",")[1];
        const imageBuffer2 = Buffer.from(base64Data2, "base64");
        const imageStream2 = bufferToStream(imageBuffer2);

        // Create FormData and append fields
        const formData = new FormData();
        formData.append("image", imageStream1, { filename: "image.jpeg" });
        formData.append("mask", imageStream2, { filename: "mask.jpeg" });
        formData.append("grow_mask", 1);
        formData.append("output_format", "webp");

        // Make the POST request with axios
        const apiResponse = await axios.post(
            `https://api.stability.ai/v2beta/stable-image/edit/erase`,
            formData,
            {
                validateStatus: undefined,
                responseType: "arraybuffer",
                headers: {
                    Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                    Accept: "image/*",
                    ...formData.getHeaders(), // Include appropriate headers for multipart data
                },
            }
        );

        // Validate API response
        if (apiResponse.status !== 200) {
            throw new Error(`${apiResponse.status}: ${Buffer.from(apiResponse.data).toString()}`);
        }

        let buffer = Buffer.from(apiResponse.data);
        const fileName = "ai" + uuidv4();

        // Process image and upload concurrently
        buffer = await compressImageWithoutHeightWidth(buffer)
        const { size, width, height, format } = await sharp(buffer).metadata();
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        const model = "erase"

        // Consolidate image info for database
        await AiImages.create({
            organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            url,
            model: model,
            dimensions: { width: width, height: height },
            format: format,
            fileName,
            fileSize: size,
        });

        // Remove credits and respond
        removeImageAiCredits(req, model);
        res.send(url)
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})


router.post("/replace/background", checkAuth, async (req, res) => {
    try {
        const { inputUrl, prompt, organizationUserId, userData, organizationId } = req.body;

        // Check credits availability
        if (!(await checkAiImageCreditsAreThere(req))) {
            return res.status(429).json({
                message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account."
            });
        }

        // Fetch the image from inputUrl as a buffer to save memory
        const imageResponse = await axios.get(inputUrl, { responseType: 'arraybuffer' })
        const imageBuffer = Buffer.from(imageResponse.data);

        // Set up form data for API request
        const formData = new FormData();
        formData.append("subject_image", imageBuffer, {
            filename: 'image.jpg',
            contentType: imageResponse.headers['content-type']
        });

        const inputImageFormat = imageResponse.headers['content-type'].split("/")[1]
        formData.append("output_format", inputImageFormat);
        formData.append("background_prompt", prompt);

        // Remove background via API call
        const response = await axios.post(`https://api.stability.ai/v2beta/stable-image/edit/replace-background-and-relight`, formData, {
            validateStatus: undefined,
            headers: {
                Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                Accept: "image/*",
                ...formData.getHeaders(), // Include appropriate headers for multipart data
            },
        })

        const result = await pollResult(response.data.id);
        if (!result) return res.status(500).json({ error: "Failed to generate image" })

        let buffer = Buffer.from(result);
        const fileName = "ai" + uuidv4();

        // Process image and upload concurrently
        buffer = await compressImageWithoutHeightWidth(buffer)
        const { size, width, height, format } = await sharp(buffer).metadata();
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        const model = "replace-background"

        // Consolidate image info for database
        await AiImages.create({
            organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            inputUrl,
            url,
            model: model,
            dimensions: { width: width, height: height },
            format: format,
            fileName,
            fileSize: size,
        });

        // Remove credits and respond
        removeImageAiCredits(req, model);
        res.send(url);
    } catch (error) {
        res.send("done")
        console.log(error)
    }
})


// Route to toggle the "favorite" status of an AI-generated image by its ID
// This route allows authenticated users to mark or unmark an image as a favorite.
// @ROUTE /muse/stability/favorites/toggle/:id
router.post("/favorites/toggle/:id", checkAuth, async (req, res) => {
    try {
        const isDataThere = await AiImages.findById(req.params.id)
        if (!isDataThere) return res.status(404).json({ message: "Image data not found" });

        if (isDataThere.isFavorite) {
            isDataThere.isFavorite = false
        } else {
            isDataThere.isFavorite = true
        }

        const data = await isDataThere.save()
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error })
    }
});

// Route to soft delete an AI-generated image by its ID
// This route marks the image as deleted instead of removing it from the database.
// @ROUTE /muse/stability/ai-image/trash/:id
router.delete("/ai-image/trash/:id", checkAuth, async (req, res) => {
    try {
        // Find the AI-generated image in the database using the provided ID
        const imageData = await AiImages.findById(req.params.id);

        // If the image is not found, return a 404 Not Found response
        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        // Mark the image as deleted (soft delete)
        imageData.isDeleted = true;
        await imageData.save();

        // Return a 200 OK response confirming the deletion
        return res.status(200).json({ message: "Image successfully deleted" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
});

// Route to permanently delete an AI-generated image by its ID
// This route removes the image from the database and deletes its associated file.
// @ROUTE DELETE /muse/stability/ai-image/:id/delete-permanently
router.delete("/ai-image/:id/delete-permanently", checkAuth, async (req, res) => {
    try {
        // Find the AI-generated image in the database using the provided ID
        const imageData = await AiImages.findById(req.params.id);

        // If the image is not found, return a 404 Not Found response
        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        await deleteOldFile(imageData.fileName);
        await AiImages.deleteOne({ _id: imageData._id });
        return res.status(200).json({ message: "Image permanently deleted." });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
});

// Route to restore a previously deleted AI-generated image
// This route marks the image as active (not deleted) in the database.
// @ROUTE POST /muse/stability/ai-image/:id/restore
router.post("/ai-image/:id/restore", checkAuth, async (req, res) => {
    try {
        // Find the AI-generated image in the database using the provided ID
        const imageData = await AiImages.findById(req.params.id);

        // If the image is not found, return a 404 Not Found response
        if (!imageData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        imageData.isDeleted = false;
        await imageData.save();

        return res.status(200).json({ message: "Image successfully deleted" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
});

// testing down
router.get("/outpaint", async (req, res) => {
    try {
        const filename = `k.jpg`;
        const directoryPath = path.join(__dirname, '..', 'stability-images', 'input-images');
        const filePath = path.join(directoryPath, filename);

        const imageMetadata = await sharp(filePath).metadata();
        const maxWidth = imageMetadata.width;
        const maxHeight = imageMetadata.height;

        // Calculate outpaint dimensions as a percentage of image dimensions (e.g., 10%)
        const outpaintPercentage = 0.1; // 10% of the image dimension
        const left = Math.floor(maxWidth * outpaintPercentage);
        const right = Math.floor(maxWidth * outpaintPercentage);
        const up = Math.floor(maxHeight * outpaintPercentage);
        const down = Math.floor(maxHeight * outpaintPercentage);

        // Create FormData and append fields
        const formData = new FormData();
        formData.append("image", fs.createReadStream(filePath));
        formData.append("output_format", "webp");
        formData.append("left", left.toString());
        formData.append("right", right.toString());
        formData.append("up", up.toString());
        formData.append("down", down.toString());


        // Make the POST request with axios
        const response = await axios.post(
            `https://api.stability.ai/v2beta/stable-image/edit/outpaint`,
            formData,
            {
                validateStatus: undefined,
                responseType: "arraybuffer",
                headers: {
                    Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                    Accept: "image/*",
                    ...formData.getHeaders(), // Include appropriate headers for multipart data
                },
            }
        );

        // Handle response
        if (response.status === 200) {
            fs.writeFileSync("./h.webp", Buffer.from(response.data));
            console.log("Image saved as husky.webp");
        } else {
            throw new Error(`${response.status}: ${response.data.toString()}`);
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})

router.post("/inpaint", async (req, res) => {
    try {
        const base64Data1 = req.body.originalImage.split(",")[1];
        const imageBuffer1 = Buffer.from(base64Data1, "base64");
        const imageStream1 = bufferToStream(imageBuffer1);

        const base64Data2 = req.body.maskImage.split(",")[1];
        const imageBuffer2 = Buffer.from(base64Data2, "base64");
        const imageStream2 = bufferToStream(imageBuffer2);

        // Create FormData and append fields
        const formData = new FormData();
        formData.append("image", imageStream1, { filename: "image.jpeg" });
        formData.append("mask", imageStream2, { filename: "image1.jpeg" });
        formData.append("prompt", req.body.prompt);
        formData.append("output_format", "webp");

        // Make the POST request with axios
        const response = await axios.post(
            `https://api.stability.ai/v2beta/stable-image/edit/inpaint`,
            formData,
            {
                validateStatus: undefined,
                responseType: "arraybuffer",
                headers: {
                    Authorization: `Bearer sk-43HrzjuiYYVbBLhJs5SWFrlR1iQpQo14CKN2wH8L9eKlJcxc`,
                    Accept: "image/*",
                    ...formData.getHeaders(), // Include appropriate headers for multipart data
                },
            }
        );

        // Handle response
        if (response.status === 200) {
            fs.writeFileSync("./h.webp", Buffer.from(response.data));
            console.log("Image saved as husky.webp");
        } else {
            throw new Error(`${response.status}: ${response.data.toString()}`);
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }
})

module.exports = router

