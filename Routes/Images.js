const express = require('express');
const axios = require('axios');
const router = express.Router();
const templateImages = require('../Schema/TemplateImages');
const { v4: uuidv4 } = require('uuid');

// logger
const logger = require('../logger');
const { downloadDriveImagesAndStoreToS3, downloadTheImageFromUrlAndStoreToS3, addBase64DataToS3Bucket } = require('../Functions/S3');

/*const getFreepikImages = async () => {
    try {
        const { data } = await axios.get('https://api.freepik.com/v1/resources', {
            headers: {
                'x-freepik-api-key': "FPSX4c221e6a59544873bba7ab5c7ec947fb"
            }
        })


        const filterImagesInObject = data.data.map(image => {
            let obj = {
                id: image.id,
                url: image.image.source.url,
                previewURL: image.image.source.url,
                width: image.image.source.size.split("x")[0],
                height: image.image.source.size.split("x")[1]
            };
            return obj;
        })

        return filterImagesInObject
    } catch (error) {
        logger.error(error);
        return [];
    }
}

router.get("/top", async (req, res) => {
    try {
        const data = await getFreepikImages()
        res.send(data)
    } catch (error) {
        res.send([])
    }
})*/

const getUnsplashImages = async (page) => {
    try {
        const photos = await axios.get(`https://api.unsplash.com/photos/?page=${page}&per_page=10`, {
            headers: {
                'Authorization': 'Client-ID d44fb5cd512707ed2cb87bcf6fed924f773ee43cc579d390cd0a01476d915bc3'
            }
        });

        const filterImagesInObject = photos.data.map(data => {
            let obj = {
                id: data.id,
                url: data.urls.regular,
                previewURL: data.urls.small,
                width: data.width,
                height: data.height
            };
            return obj;
        });

        return filterImagesInObject;
    } catch (error) {
        logger.error(error);
        return [];
    }
};


const getPexelsImages = async (page) => {
    try {
        const photos = await axios.get(`https://api.pexels.com/v1/curated?page=${page}&per_page=10`, {
            headers: {
                "Authorization": "563492ad6f91700001000001c8495ed795a0477fb491bb09364ae89f"
            }
        });

        const filterImagesInObject = photos.data.photos.map(data => {
            let obj = {
                id: data.id,
                url: data.src.portrait,
                previewURL: data.src.tiny,
                width: data.width,
                height: data.height
            };
            return obj;
        });

        return filterImagesInObject;
    } catch (error) {
        logger.error(error);
        return [];
    }
};


router.get('/', async (req, res) => {
    const unsplashImages = await getUnsplashImages(req.query.page);
    const pexelsImages = await getPexelsImages(req.query.page);

    res.send([...unsplashImages, ...pexelsImages]);
});


const searchUnsplashPhotos = async (query, page) => {
    try {
        query = query === "" ? undefined : query
        const photos = await axios.get(`https://api.unsplash.com/search/photos?query=${query}&page=${page}&per_page=10`, {
            headers: {
                'Authorization': 'Client-ID d44fb5cd512707ed2cb87bcf6fed924f773ee43cc579d390cd0a01476d915bc3'
            }
        });

        const filterImagesInObject = photos.data.results.map(filterImageProperties);
        return filterImagesInObject;
    } catch (error) {
        logger.error(error);
        return []
    }
};

const searchPexelsImages = async (query, page) => {
    try {
        const photos = await axios.get(`https://api.pexels.com/v1/curated?q=${query}&page=${page}&per_page=10`, {
            headers: {
                "Authorization": "563492ad6f91700001000001c8495ed795a0477fb491bb09364ae89f"
            }
        });

        const filterImagesInObject = photos.data.photos.map(filterImageProperties);
        return filterImagesInObject;
    } catch (error) {
        logger.error(error);
        return [];
    }
};

// Using lodash to simplify the mapping
const filterImageProperties = (data) => {
    const obj = {
        id: data.id,
        url: data.urls?.regular || data.largeImageURL || data.src?.large2x,
        previewURL: data.urls?.thumb || data.previewURL || data.src?.medium,
        width: data.width || data.imageWidth,
        height: data.height || data.imageHeight
    };
    return obj;
};

router.get('/search', async (req, res) => {
    const { query = "", page = 0 } = req.query

    let unsplashImages = []
    let pexelsImages = []

    if (query === "") {
        const [unsplashImages1, pexelsImages1] = await Promise.all([
            getUnsplashImages(page),
            getPexelsImages(page),
        ]);

        unsplashImages = unsplashImages1
        pexelsImages = pexelsImages1
    } else {
        const [unsplashImages1, pexelsImages1] = await Promise.all([
            searchUnsplashPhotos(query, page),
            searchPexelsImages(query, page),
        ]);

        unsplashImages = unsplashImages1
        pexelsImages = pexelsImages1
    }


    res.send([...unsplashImages, ...pexelsImages]);
});

// @Route /muse/images/download/generate/drive/url
// @desc drive images and upload to s3 
router.post('/download/generate/drive/url', async (req, res) => {
    try {
        const url = await templateImages.findOne({ originalURL: req.body.imageUrl });
        if (url !== null) {
            let urlObj = {
                URL: url.s3URL
            };
            res.send(urlObj);
        } else {
            const data = await downloadDriveImagesAndStoreToS3(req.body.fileId, req.body.access_token, req.body.mimeType, req.body.size);
            const imageData = {
                userId: req.body.userId,
                originalURL: req.body.imageUrl,
                s3URL: data.URL,
                fileName: req.body.fileName,
            };
            await templateImages.create(imageData);
            if (data === "invalid url") return res.status(400).json({
                error: 'Unable to process'
            });

            res.send(data);
        }
    } catch (error) {
        res.status(500).json({
            error: error
        });
    }
});



// @Route /muse/images/download/generate/url
// @desc download url and upload to s3 google photos
router.post('/download/generate/url', async (req, res) => {
    try {
        const url = await templateImages.findOne({ originalURL: req.body.originalUrl });
        if (url !== null) {
            let imageObj = {
                oldURL: url.originalURL,
                newURL: url.s3URL
            };
            res.send(imageObj);
        } else {
            const data = await downloadTheImageFromUrlAndStoreToS3(req.body.originalUrl, req.body.userId);
            if (data === "invalid url") return res.status(400).json({
                error: 'Unable to process'
            });
            res.send(data);
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error
        });
    }
});

// @Route /muse/images/download/generate/dropbox/url
// @desc dropbox images and upload to s3 
router.post('/download/generate/dropbox/url', async (req, res) => {
    try {
        const data = await addBase64DataToS3Bucket(req.body.base64);
        let imageObj = {
            id: uuidv4(),
            url: data
        };
        res.send(imageObj);
    } catch (error) {
        res.status(500).json({
            error: error
        });
    }
});





module.exports = router

