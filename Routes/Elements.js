const express = require('express')
const axios = require('axios')
const router = express.Router()
let token = ""
// logger
const logger = require('../logger');

const getTheSwaggerElements = async () => {
    try {
        const data = await axios.get('https://api.iconscout.com/v3/search?query=&product_type=item&asset=icon&per_page=10&page=1&sort=relevant', {
            headers: {
                "Client-ID": "242360359370022"
            }
        })
        return data.data.response.items.data
    } catch (error) {
        logger.error(error)
    }
}

const authorizeFlatIcons = async () => {
    try {
        const response = await axios.post('https://api.flaticon.com/v3/app/authentication', {
            apikey: 'ed9c5d3e3fce3d68c21ce3f9a8d74a1652ba79fc'
        })
        token = response.data.data.token
    } catch (error) {
        logger.error(error)
    }
}

const getFlatIcons = async (page) => {
    try {
        const data = await axios.get(`https://api.flaticon.com/v3/items/icons?page=${page}&limit=30`, {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
        return data.data.data.map(svg => {
            let obj = {
                id: svg.id,
                svg: svg.images.svg
            }
            return obj
        })
    } catch (error) {
        logger.error(error)
        return null
    }
}

const searchFlaticons = async (search, page) => {
    try {
        const res = await axios.get(`https://api.flaticon.com/v3/search/icons?q=${search}&page=${page}&limit=30`, {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })


        return res.data.data.map(svg => {
            let obj = {
                id: svg.id,
                svg: svg.images.svg
            }
            return obj
        })
    } catch (error) {
        logger.error(error)
        return null
    }
}

router.get('/', async (req, res) => {
    try {
        let data = await getFlatIcons(req.query.page)
        if (data === null) {
            await authorizeFlatIcons()
            data = await getFlatIcons(req.query.page)
        }
        res.send(data)
    } catch (error) {
        logger.error(error)
    }
})

router.get('/search', async (req, res) => {
    try {
        await authorizeFlatIcons()
        let data = await searchFlaticons(req.query.query, req.query.page)
        if (data === null) {
            await authorizeFlatIcons()
            data = await searchFlaticons(req.query.query, req.query.page)
        }
        res.send(data)
    } catch (error) {
        logger.error(error)
    }
})

module.exports = router