const express = require('express')
const { downloadTheImageFromUrlAndStoreToS3, addBase64DataToS3Bucket, deleteOldFile } = require('../Functions/S3')
const router = express.Router()
const templateImages = require('../Schema/TemplateImages')
const template = require('../Schema/TemplateSchema')
// logger
const logger = require('../logger');
const adminTemplate = require('../Schema/AdminTemplateSchema')
const mongoose = require('mongoose')
const checkAuth = require('../Middleware/CheckAuth')

router.get('/user/template/count/:userId', async (req, res) => {
    try {
        const data = await template.countDocuments({ userId: req.params.userId })
        res.status(200).json({
            templates: data
        })
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get('/:id', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const searchQuery = req.query.search || ''; // Get the search query from the request

        let templateData;
        let defaultTemplates = []

        if (searchQuery) {
            // If search query is provided, filter by template name
            templateData = await template.find({
                organizationId: req.params.id,
                isDeleted: { $ne: true },
                projectName: { $regex: new RegExp(searchQuery, 'i') } // Case-insensitive partial matching
            }).skip(skip).limit(limit).sort({ updatedAt: -1 });
        } else {
            // If no search query, fetch all templates
            templateData = await template.find({ organizationId: req.params.id, isDeleted: { $ne: true } }).skip(skip).limit(limit).sort({ updatedAt: -1 })

            if (templateData.length < limit) {
                defaultTemplates = await adminTemplate.find({
                    '_id': {
                        $in: [
                            mongoose.Types.ObjectId('61e7b2f566319900094aee1a'),
                            mongoose.Types.ObjectId('61ed4d00d08b1300097077da'),
                            mongoose.Types.ObjectId('61ed48177acfaf00098ef24d'),
                            mongoose.Types.ObjectId('61e7dc291485b900099da76b')
                        ]
                    }
                })
            }
        }

        res.send([...templateData, ...defaultTemplates])
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error
        })
    }
})


router.get('/:id/:templateType', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        const searchQuery = req.query.search || ''; // Get the search query from the request

        let data;

        if (searchQuery) {
            // If search query is provided, filter by template name
            data = await template.find({
                organizationId: req.params.id,
                templateType: req.params.templateType,
                projectName: { $regex: new RegExp(searchQuery, 'i') } // Case-insensitive partial matching
            }).skip(skip).limit(limit).sort({ updatedAt: -1 });
        } else {
            // If no search query, fetch all templates
            data = await template.find({
                organizationId: req.params.id,
                templateType: req.params.templateType
            }).skip(skip).limit(limit).sort({ updatedAt: -1 });
        }

        res.send(data)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error
        })
    }
})

const getTheMediaFromArray = (data) => {
    const imagesArray = []
    const CanvasData = JSON.parse(data.template)
    CanvasData.forEach((objects) => {
        objects.arrayOfElements.forEach(element => {
            if (element.type === 'image') {
                imagesArray.push(element.src)
            } else if (element.type === 'svg') {
                imagesArray.push(element.src)
            } else if (element.type === 'mask') {
                imagesArray.push(element.src)
                imagesArray.push(element.src1)
            } else if (element.type === 'group') {
                element.data.forEach(groupElement => {
                    if (groupElement.type === 'image') {
                        imagesArray.push(groupElement.src)
                    } else if (groupElement.type === 'svg') {
                        imagesArray.push(groupElement.src)
                    } else if (groupElement.type === 'mask') {
                        imagesArray.push(groupElement.src)
                        imagesArray.push(groupElement.src1)
                    }
                })
            }
        })
    })
    return imagesArray
}


const changeTheSrcOfMedia = async (data, newURL, dataBaseData) => {
    try {
        let CanvasData = JSON.parse(data.template)
        newURL.forEach(data => {
            CanvasData.forEach((objects) => {
                objects.arrayOfElements.forEach(element => {
                    if (element.type === 'image') {
                        if (data.oldURL !== element.src) return
                        element.src = data.newURL
                    } else if (element.type === 'svg') {
                        if (data.oldURL !== element.src) return
                        element.src = data.newURL
                    } else if (element.type === 'mask') {
                        if (data.oldURL === element.src) {
                            element.src = data.newURL
                        }
                        if (data.oldURL === element.src1) {
                            element.src1 = data.newURL
                        }
                    } else if (element.type === 'group') {
                        element.data.forEach(groupElement => {
                            if (groupElement.type === 'image') {
                                if (data.oldURL !== groupElement.src) return
                                groupElement.src = data.newURL
                            } else if (groupElement.type === 'svg') {
                                if (data.oldURL !== groupElement.src) return
                                groupElement.src = data.newURL
                            } else if (groupElement.type === 'mask') {
                                if (data.oldURL === groupElement.src) {
                                    groupElement.src = data.newURL
                                }
                                if (data.oldURL === groupElement.src1) {
                                    groupElement.src1 = data.newURL
                                }
                            }
                        })
                    }
                })
            })
        })

        dataBaseData.template = JSON.stringify(CanvasData)
        await dataBaseData.save()
    } catch (error) {
        logger.error(error)
    }
}


const mapTheObjectToDownload = async (data, dataBaseData) => {
    try {
        let newURL = []
        let media = getTheMediaFromArray(data)
        media = [...new Set(media)];
        for await (const url of media) {
            const mediaIsThere = await templateImages.findOne({ originalURL: url })
            if (mediaIsThere === null) {
                const object = await downloadTheImageFromUrlAndStoreToS3(url, data.userId)
                newURL.push(object)
            } else {
                newURL.push({
                    oldURL: mediaIsThere.originalURL,
                    newURL: mediaIsThere.s3URL
                })
            }
        }
        changeTheSrcOfMedia(data, newURL, dataBaseData)
    } catch (error) {
        logger.error(error)
    }
}

router.post('/', async (req, res) => {
    try {
        let CanvasData = JSON.parse(req.body.template)
        const urls = []
        for await (const base64 of CanvasData) {
            let data = await addBase64DataToS3Bucket(base64.image)
            let obj = {
                smallImage: data
            }
            urls.push(obj)
        }

        // add the preview url
        let previewData = CanvasData.map((data, index) => {
            data.image = urls[index].smallImage
            return data
        })

        //updated the added url in template
        req.body.template = JSON.stringify(previewData)
        const data = await template.create(req.body)
        res.status(201).json({
            message: 'done',
            id: data._id
        })

        mapTheObjectToDownload(req.body, data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

router.post('/update', async (req, res) => {
    try {
        let data
        let adminTemplateId = ['61e7b2f566319900094aee1a', '61ed4d00d08b1300097077da', '61ed48177acfaf00098ef24d', '61e7dc291485b900099da76b']

        if (adminTemplateId.includes(req.body.id)) {
            data = await adminTemplate.findById(req.body.id)
        } else {
            data = await template.findById(req.body.id)
            JSON.parse(data.template).forEach(elements => {
                deleteOldFile(elements.image.split('/').pop())
            })
        }

        let CanvasData = JSON.parse(req.body.template)
        const urls = []

        for await (const base64 of CanvasData) {
            let data = await addBase64DataToS3Bucket(base64.image)
            let obj = {
                smallImage: data
            }
            urls.push(obj)
        }

        // add the preview url
        let previewData = CanvasData.map((data, index) => {
            data.image = urls[index].smallImage
            return data
        })

        //updated the added url in template
        req.body.template = JSON.stringify(previewData)

        data.projectName = req.body.projectName
        data.template = req.body.template
        data.userName = req.body.userName
        data.canvasMultiply = req.body.canvasMultiply
        let newData

        if (adminTemplateId.includes(req.body.id)) {
            data.adminTemplateId = data._id
            let updatedData = new template({
                organizationId: req.body.organizationId,
                userId: req.body.userId,
                userName: req.body.userName,
                template: data.template,
                canvasWidth: req.body.canvasWidth,
                canvasHeight: req.body.canvasHeight,
                canvasMultiply: req.body.canvasMultiply,
                projectName: req.body.projectName,
                templateType: req.body.templateType,
                adminTemplateId: data._id.toString(),
            })
            newData = await updatedData.save()
        } else {
            newData = await data.save()
        }

        res.status(201).send("done")
        mapTheObjectToDownload(req.body, newData)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})


router.get("/edit/:organizationId/:id", checkAuth, async (req, res) => {
    const { organizationId, id } = req.params;

    try {
        let data = await template.findOne({ _id: id });

        if (!data) {
            data = await adminTemplate.findOne({ _id: id });
        }

        if (!data) {
            return res.status(404).json({ error: "Template not found" });
        }

        res.json(data);
    } catch (error) {
        res.status(500).json({ error: error?.message || "Internal Server Error" });
    }
});



router.delete('/:id', checkAuth, async (req, res) => {
    try {
        await template.deleteOne({ _id: req.params.id })
        res.send("done")
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

// get designs which are in trash
router.get('/design/trash/:id', checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        const templateData = await template.find({ organizationId: req.params.id, isDeleted: true }).skip(skip).limit(limit).sort({ updatedAt: -1 })
        res.send(templateData)
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
})

// user template add to trash
router.delete('/trash/:id', checkAuth, async (req, res) => {
    try {
        const templateData = await template.findById(req.params.id);

        if (!templateData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        // Mark the template as deleted (soft delete)
        templateData.isDeleted = true;
        await templateData.save();

        // Return a 200 OK response confirming the deletion
        return res.status(200).json({ message: "template successfully deleted" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
})

// user template add to trash
router.post('/design/restore/:id', checkAuth, async (req, res) => {
    try {
        const templateData = await template.findById(req.params.id);

        if (!templateData) {
            return res.status(404).json({ message: "Image data not found" });
        }

        templateData.isDeleted = false;
        await templateData.save();

        // Return a 200 OK response confirming the deletion
        return res.status(200).json({ message: "template removed from trash" });
    } catch (error) {
        // Return a 500 Internal Server Error response if something goes wrong
        return res.status(500).json({ message: "An error occurred while deleting the image", error: error.message });
    }
})

router.put("/favorites/toggle/:id", checkAuth, async (req, res) => {
    try {
        const isDataThere = await template.findById(req.params.id)
        if (!isDataThere) return res.status(404).json({ message: "template not found" });

        if (isDataThere.isFavorite) {
            isDataThere.isFavorite = false
        } else {
            isDataThere.isFavorite = true
        }

        const data = await isDataThere.save()
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error })
    }
});

router.get("/favorites/:id", checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 25
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const userUploadsImages = await template.find({ organizationId: req.params.organizationId, isFavorite: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(userUploadsImages)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: "Internal server error"
        })
    }
});

module.exports = router