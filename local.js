const app = require('./index');
const logger = require('./logger');

app.listen(4003 || process.env.PORT, () => {
  logger.info('muse running');
});
