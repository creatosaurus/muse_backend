const mongoose = require('mongoose')
const schema = mongoose.Schema

const userStorage = new schema({
    userId: String,
    storageSize: String
}, { timestamps: true })

module.exports = mongoose.model('UserStorage', userStorage)