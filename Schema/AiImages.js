const mongoose = require('mongoose');

const aiImageSchema = new mongoose.Schema({
    organizationUserId: String,
    organizationId: String,
    createdBy: String,
    prompt: {
        type: String,
        trim: true,
    },
    inputUrl: {
        type: String,
        trim: true,
    },
    url: {
        type: String,
        required: true,
        trim: true,
    },
    model: {
        type: String,
        required: true,
    },
    type: String,
    isPublic: {
        type: Boolean,
        default: false
    },
    dimensions: {
        width: Number,
        height: Number,
    },
    format: {
        type: String,
        enum: ['jpeg', 'png', 'webp', 'jpg'],
        required: true,
    },
    fileName: String,
    fileSize: Number,
    isDeleted: Boolean,
    isFavorite: Boolean
}, { timestamps: true });

module.exports = mongoose.model('AIImage', aiImageSchema);
