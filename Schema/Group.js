const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    tags: [{
        type: String,
        lowercase: true,
        trim: true
    }],
    rank: {
        type: Number,
        default: 0
    },
    templateType: {
        type: String
    },
    templateIds: [{
        type: String,
        lowercase: true,
        trim: true,
        default: []
    }]
});

module.exports = mongoose.model('group', groupSchema);
